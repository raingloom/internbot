extern crate mammut;
use mammut::Registration;
use mammut::apps::{AppBuilder, Scope};

fn main() {
   run().unwrap();
}

fn run() -> mammut::Result<()> {
    let app = AppBuilder {
        client_name: "internbot",
        redirect_uris: "urn:ietf:wg:oauth:2.0:oob",
        scopes: Scope::Read,
        website: None,
    };

    let mut registration = Registration::new("https://botsin.space");
    registration.register(app)?;
    let url = registration.authorise()?;
    // Here you now need to open the url in the browser
    // And handle a the redirect url coming back with the code.
    let code = String::from("RETURNED_FROM_BROWSER");
    let mastodon = registration.create_access_token(code)?;

    println!("{:?}", mastodon.get_home_timeline()?);
    Ok(())
}

